<?php
require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'].'/assets/php/manager/Parking.php';
require $_SERVER['DOCUMENT_ROOT'].'/assets/php/manager/Controller.php';
require $_SERVER['DOCUMENT_ROOT'].'/assets/php/session/SessionController.php';

use Parse\ParseUser;
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseException;
use Parse\ParseQuery;
use Parse\ParseGeoPoint;

session_start();
ParseClient::initialize('1WL6NbyRiYpMMyD4Hg6lXbrRRGbsd5JoWMSZtRcl', 'F3KKmjkCWmKLHBhSf3gtVjKitlXZW6xcCCujQOzz', 'XLQogDE64ksqB7XsJnVh0Ccjwp0WO2D0XRJmmRG0');

if( $_POST['parking'] )
{
    $action = (int)$_POST['parking'];
    
    switch($action)
    {
        case 1:
            $result = array();
            $resultsParking = array();
            $resultsControllers = array();
            $session = unserialize(base64_decode($_SESSION['session']) );
            $manager = $session->manager;
            $queryParking = new ParseQuery('Parqueadero');
            $queryParking->equalTo('owner', $manager);
            $queryControllers = new ParseQuery('ParkingControllers');
            $queryControllers->equalTo('manager', $manager);
            $result['error'] = false;
            try
            {
                $queryResults = $queryParking->find();
                for( $i = 0; $i < count($queryResults); ++$i )
                {
                    $parseParking = $queryResults[$i];
                    $newParking = new Parking;
                    $newParking->name = $parseParking->get('nombre');
                    $newParking->coordinates = $parseParking->get('coordenadas');
                    $newParking->price = $parseParking->get('tarifa');
                    $newParking->totalSlots = $parseParking->get('totalCupos');
                    $newParking->reservableSlots = $parseParking->get('cuposReservables');
                    $newParking->availableSlots = $parseParking->get('cuposDisponibles');
                    $newParking->availableReservableSlots = $parseParking->get('cuposReservablesDisponibles');
                    array_push($resultsParking, $newParking);
                }
                $queryResults = $queryControllers->find();
                for($i = 0; $i < count($queryResults); ++$i) {
                    $parseController = $queryResults[$i];
                    $controllerUserQuery = new ParseQuery('_User');
                    $parseControllerUser = $controllerUserQuery->get( $parseController->get('parkingController')->getObjectId() );
                    $controllerUser = new Controller;
                    $controllerUser->username = $parseControllerUser->get('username');
                    $controllerUser->firstName = $parseControllerUser->get('firstName');
                    $controllerUser->lastName = $parseControllerUser->get('lastName');
                    array_push($resultsControllers, $controllerUser);
                }
                
                $result['parking'] = $resultsParking;
                $result['controllers'] = $resultsControllers;
                echo json_encode($result);
            }
            catch( ParseException $error )
            {
                echo $error->getMessage();
            }
            break;
            
        case 2:
            $result = array();
            $correctInput = $_POST['name'] && $_POST['coordinates'] && $_POST['price'] && $_POST['totalSlots'] && $_POST['reservableSlots'];
            
            $result['error'] = false;
            
            if( $correctInput )
            {
                $session = unserialize(base64_decode($_SESSION['session']) );
                $price = (double)$_POST['price'];
                $totalSlots = (int)$_POST['totalSlots'];
                $reservableSlots = (int)$_POST['reservableSlots'];
                $numberCoordinates = array();
                $stringCoordinates = $_POST['coordinates'];
                
                for( $i = 0; $i < count($stringCoordinates); ++$i )
                {
                    $lat = floatval($stringCoordinates[$i][0]);
                    $lng = floatval($stringCoordinates[$i][1]);
                    array_push($numberCoordinates, array($lat, $lng) );
                }
                
                $centroidLat = 0.0;
                $centroidLng = 0.0;
                $signedArea = 0.0;
                $x0 = 0.0;
                $y0 = 0.0;
                $x1 = 0.0;
                $y1 = 0.0;
                $a = 0.0;
                
                for($i = 0; $i < count($numberCoordinates) - 1; ++$i) {
                    $x0 = $numberCoordinates[$i][0];
                    $y0 = $numberCoordinates[$i][1];
                    $x1 = $numberCoordinates[$i+1][0];
                    $y1 = $numberCoordinates[$i+1][1];
                    $a = $x0 * $y1 - $x1 * $y0;
                    $signedArea += $a;
                    $centroidLat += ($x0 + $x1) * $a;
                    $centroidLng += ($y0 + $y1) * $a;
                }
                $x0 = $numberCoordinates[$i][0];
                $y0 = $numberCoordinates[$i][1];
                $x1 = $numberCoordinates[0][0];
                $y1 = $numberCoordinates[0][1];
                $a = $x0 * $y1 - $x1 * $y0;
                $signedArea += $a;
                $centroidLat += ($x0 + $x1) * $a;
                $centroidLng += ($y0 + $y1) * $a;
                $signedArea *= 0.5;
                $centroidLat /= (6.0*$signedArea);
                $centroidLng /= (6.0*$signedArea);
                
                $centroid = new ParseGeoPoint($centroidLat, $centroidLng);
                
                $newParking = new ParseObject('Parqueadero');
                $newParking->set( 'nombre', $_POST['name'] );
                $newParking->setArray( 'coordenadas', $numberCoordinates );
                $newParking->set( 'centroide', $centroid );
                $newParking->set( 'tarifa', $price );
                $newParking->set( 'totalCupos',  $totalSlots);
                $newParking->set( 'cuposReservables', $reservableSlots );
                $newParking->set( 'cuposDisponibles', $totalSlots - $reservableSlots );
                $newParking->set( 'owner', $session->manager );
                try
                {
                    $newParking->save();
                }
                catch (ParseException $ex)
                {
                    $result['error'] = true;
                    $result['details'] = $ex->getMessage();
                }
            }
            else
            {
                $result['error'] = true;
                $result['details'] = 'Incorrect input.';
            }
            
            echo json_encode($result);
            
            break;
            
        case 3:
            $results = array();
            $results['error'] = false;
            
            $password = $_POST['password'];
            $confirmPassword = $_POST['confirmPassword'];
            $session = unserialize(base64_decode($_SESSION['session']) );
            $manager = $session->getManager();
            
            if (strlen($password) < 6) {
                $results['error'] = true;
                $results['details'] = 'Password must have 6 characters or more';
            } else if($password != $confirmPassword) {
                $results['error'] = true;
                $results['details'] = 'Passwords are not equal';
            } else if($manager == null) {
                $results['error'] = true;
                $results['details'] = 'You are not a manager';
            } else {
                
                
                $newParkingControllerUser = new ParseObject('_User');
                $newParkingControllerUser->set('username', $_POST['username'] );
                $newParkingControllerUser->set('password', $_POST['password'] );
                $newParkingControllerUser->set('firstName', $_POST['firstName'] );
                $newParkingControllerUser->set('lastName', $_POST['lastName'] );
                
                $newParkingController = new ParseObject('ParkingControllers');
                $newParkingController->set('manager', $manager);
                
                try {
                    $newParkingControllerUser->save();
                    $newParkingController->set('parkingController', $newParkingControllerUser);
                    $newParkingController->save();
                } catch(ParseException $ex) {
                    $results['error'] = true;
                    $results['details'] = $ex->getMessage();
                }
            }
            
            echo json_encode($results);
            break;
    }
}
?>