<?php
require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

use Parse\ParseUser;
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseException;
use Parse\ParseQuery;

session_start();
ParseClient::initialize('1WL6NbyRiYpMMyD4Hg6lXbrRRGbsd5JoWMSZtRcl', 'F3KKmjkCWmKLHBhSf3gtVjKitlXZW6xcCCujQOzz', 'XLQogDE64ksqB7XsJnVh0Ccjwp0WO2D0XRJmmRG0');

class SessionController {
    var $user = null;
    var $manager = null;
    var $controller = null;
    var $cParking = null;
    
    public function signUpManager( $data ) {
        $results = array();
        $results['error'] = false;
        
        $newUser = new ParseUser;
        $newUser->set('username', $data['user'] );
        $newUser->set('password', $data['password'] );
        $newUser->set('email', $data['email'] );
        $newUser->set('firstName', $data['firstName'] );
        $newUser->set('lastName', $data['lastName'] );
        
        $newManager = ParseObject::create("Manager");
        $newManager->set('companyName', $data['companyName']);
        $newManager->set('telephone', $data['telephone']);
        
        try {
            $newUser->signUp();
            $newManager->set('basicInformation', $newUser );
            $newManager->save();
            $this->user = $newUser;
            $this->manager = $newManager;
        } catch( ParseException $ex ) {
            $results['error'] = true;
            $results['details'] = $ex->getMessage();
            $newUser->logOut();
        }
        
        return $results;
    }
    
    public function LogIn($user, $password) {
        $result = array();
        $result['error'] = false;
        $queryManager = new ParseQuery('Manager');
        $queryController = new ParseQuery('ParkingControllers');
        
        try {
            $this->user = ParseUser::logIn($user, $password);
            $result['type'] = 0;
            $queryManager->equalTo('basicInformation', $this->user);
            if( $queryManager->count() > 0 ) {
                $this->manager = $queryManager->first();
                $result['type'] = 1;
            }
            $queryController->equalTo('parkingController', $this->user);
            if($queryController->count() > 0) {
                $this->controller = $queryController->first();
                $result['type'] = 2;
                $managerController = $this->controller->get('manager');
                $parkingQuery = new ParseQuery('Parqueadero');
                $parkingQuery->equalTo('owner', $managerController);
                $this->cParking = $parkingQuery->first();
            }
        } catch (ParseException $error) {
            $result['error'] = true;
            $result['details'] = $error->getMessage();
        }
        return $result;
    }
    
    public function LogOut() {
        $result = array();
        $result['error'] = false;
        
        if( $this->user ) {
            $this->user->logOut();
            $this->user = null;
            $this->manager = null;
            $this->controller = null;
            $this->cParking = null;
        } else {
            $result['error'] = true;
            $result['details'] = 'No hay una sesión iniciada.';
        }
        return $result;
    }
    
    public function getManager() {
        return $this->manager;
    }
}

if( $_POST['action'] )
{
    $action = (int)$_POST['action'];
    $result = null;
    $session = null;
    
    switch($action)
    {
        case 1:
            $session = unserialize(base64_decode($_SESSION['session']) );
            $result = $session->LogOut();
            $session = null;
            $_SESSION['session'] = base64_encode( serialize( $session ) );
            break;
        case 2:
            if( $_POST['user'] && $_POST['password'] ) {
                $user = $_POST['user'];
                $password = $_POST['password'];
                $session = new SessionController;
                $result = $session->LogIn($user, $password);
                $_SESSION['session'] = base64_encode( serialize( $session ) );
            } else {
                $result['error'] = true;
                $result['details'] = 'Parametros incompletos.';
            }
            
            break;
    }
    echo json_encode($result);
}
?>