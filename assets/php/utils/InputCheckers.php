<?php

abstract class InputChecker
{
    abstract function __construct($JSONRequest);
    abstract public function CheckInput();
}

class RegisterManagerChecker extends InputChecker
{
    var $user;
    var $password;
    var $confirmPassword;
    var $firstName;
    var $lastName;
    var $companyName;
    var $telephone;
    var $email;
    var $terms;
    
    function __construct($JSONRequest)
    {
        $this->user = $JSONRequest['user'];
        $this->password = $JSONRequest['password'];
        $this->confirmPassword = $JSONRequest['confirmPassword'];
        $this->firstName = $JSONRequest['firstName'];
        $this->lastName = $JSONRequest['lastName'];
        $this->companyName = $JSONRequest['companyName'];
        $this->telephone = $JSONRequest['telephone'];
        $this->email = $JSONRequest['email'];
        $this->terms = $JSONRequest['terms'] === 'on' ? true : false;
    }
    
    public function CheckInput()
    {
        $result = array();
        $result['error'] = false;
        $formatResult = $this->WrongFormat();
        
        if ( count($formatResult) != 0 )
        {
            $result['error'] = true;
            $result['details'] = $formatResult;
        }
        
        return $result;
    }
    
    private function WrongFormat()
    {
        $results = array();
        
        if( strlen($this->user) < 5 || strlen($this->user) > 16 ) { $result['user'] = 0; }
        if( strlen($this->password) < 6 ) { $results['password'] = 0; }
        if( $this->password != $this->confirmPassword ) { $results['confirmPassword'] = 0; };
        if( empty($this->firstName) ) { $results['firstName'] = 0; }
        if( empty($this->lastName) ) { $results['lastName'] = 0; }
        if( empty($this->companyName) ) { $results['companyName'] = 0; }
        if( empty($this->telephone) ) { $results['telephone'] = 0; }
        if( !is_numeric($this->telephone) ) { $results['telephone'] = 1; }
        if( !filter_var($this->email, FILTER_VALIDATE_EMAIL) ) { $results['email'] = 0; }
        if( !($this->terms) ) { $results['terms'] = 0; }
        
        return $results;
    }
}

?>