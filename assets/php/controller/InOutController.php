<?php
require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'].'/assets/php/session/SessionController.php';

use Parse\ParseUser;
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseException;
use Parse\ParseQuery;

session_start();
ParseClient::initialize('1WL6NbyRiYpMMyD4Hg6lXbrRRGbsd5JoWMSZtRcl', 'F3KKmjkCWmKLHBhSf3gtVjKitlXZW6xcCCujQOzz', 'XLQogDE64ksqB7XsJnVh0Ccjwp0WO2D0XRJmmRG0');

$response['error'] = false;

function throwError($details) {
    global $response;
    $response['error'] = true;
    $response['details'] = $details;
}

function increaseRating($ratingParse) {
    $rating = $ratingParse->get('rating');
    $rating += 0.2;
    if ($rating > 10.0) {
        $rating = 10.0;
    }
    $ratingParse->set('rating', $rating);
    $ratingParse->save();
}

function decreaseRating($ratingParse) {
    $rating = $ratingParse->get('rating');
    $rating -= 0.2;
    if ($rating < 0.0) {
        $rating = 0.0;
    }
    $ratingParse->set('rating', $rating);
    $ratingParse->save();
}

function changeReservation($state) {
    if ($_POST['reservationID']) {
        $reservationID = $_POST['reservationID'];
        $queryReservation = new ParseQuery('Reserva');
        $queryRating = new ParseQuery('Conductor');
        try {
            $reservationParse = $queryReservation->get($reservationID);
            $reservationParse->set('estado', $state);
            $reservationParse->save();
            
            $driver = $reservationParse->get('cliente');
            $queryRating->equalTo('usuario', $driver);
            $ratingParse = $queryRating->first();
            switch($state) {
                case 1:
                    $arriveBlock = $reservationParse->get('bloqueInicio');
                    $arriveBlock->fetch();
                    $arriveHour = $arriveBlock->get('horaInicio');
                    $currentTime = new DateTime();
                    $arriveHour->setDate($currentTime->format('Y'), $currentTime->format('m'), $currentTime->format('d'));
                    if ($currentTime < $arriveHour) {
                        increaseRating($ratingParse);
                    }  else {
                        decreaseRating($ratingParse);
                    }
                break;
                case 2:
                    $departureBlock = $reservationParse->get('bloqueFin');
                    $departureBlock->fetch();
                    $departureHour = $departureBlock->get('horaFin');
                    $currentTime = new DateTime();
                    $departureHour->setDate($currentTime->format('Y'), $currentTime->format('m'), $currentTime->format('d'));
                    if ($currentTime < $departureHour) {
                        increaseRating($ratingParse);
                    }  else {
                        decreaseRating($ratingParse);
                    }
                break;
            }
        } catch (ParseException $ex) {
            throwError($ex->getMessage() );
        }
    } else {
        throwError('Bad defined request. Expected: reservationID.');
    }
}

function createStaying() {
    if ($_POST['exitHour'] && $_POST['userID']) {
        $session = unserialize(base64_decode($_SESSION['session']) );
        $userID = $_POST['userID'];
        $userQuery = new ParseQuery('_User');
        try {
            $accountUser = $userQuery->get($userID);
            $exitHour = $_POST['exitHour'];
            $cParking = $session->cParking;
            $exitDate = new DateTime();
            $exitDate->setTime($exitHour['hour']+5, $exitHour['minute']);
            
            $newStaying = new ParseObject('Estadia');
            $newStaying->set('parqueadero', $cParking);
            $newStaying->set('horaSalida', $exitDate);
            $newStaying->set('usuario', $accountUser);
            
            $newStaying->save();
        } catch (ParseException $ex) {
            throwError($ex->getMessage());
        }
    } else {
        throwError('Bad defined request. Expected: exitHour and userID.');
    }
}

function deleteStaying() {
    if ($_POST['userID']) {
        $userID = $_POST['userID'];
        $userQuery = new ParseQuery('_User');
        $stayingQuery = new ParseQuery('Estadia');
        try {
            $user = $userQuery->get($userID);
            $stayingQuery->equalTo('usuario', $user);
            $allStaying = $stayingQuery->find();
            for($i = 0; $i < count($allStaying); ++$i) {
                $allStaying[$i]->destroy();
            }
        } catch (ParseException $ex) {
            throwError($ex->getMessage());
        }
    }
}

if ($_POST['controller']) {
    $action = (int)$_POST['controller'];
    
    switch($action) {
        case 1:
            if ($_POST['typeScan']) {
                $typeScan = $_POST['typeScan'];
                switch($typeScan) {
                    case 'RIN':
                        changeReservation(1);
                    break;
                    case 'ROUT':
                        changeReservation(2);
                    break;
                    case 'SIN':
                        createStaying();
                    break;
                    case 'SOUT':
                        deleteStaying();
                    break;
                    default:
                        throwError("Bad typeScan value $typeScan.");
                }
            } else {
                throwError('Bad defined request. Expected: typeScan.');
            }
        break;
    }
} else {
    throwError('Bad defined request. Expected: controller.');
}

echo json_encode($response);

?>