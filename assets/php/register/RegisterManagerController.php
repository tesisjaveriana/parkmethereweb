<?php
require $_SERVER['DOCUMENT_ROOT'].'/assets/php/utils/InputCheckers.php';
require $_SERVER['DOCUMENT_ROOT'].'/assets/php/session/SessionController.php';

use Parse\ParseUser;
use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseException;

session_start();

$inputChecker = new RegisterManagerChecker($_POST);
$checkResults = $inputChecker->CheckInput();
 
if( !$checkResults['error'] )
{
    $newSession = new SessionController;
    $checkResults = $newSession->signUpManager($_POST);
    $_SESSION['session'] = base64_encode(serialize($newSession) );
}

echo json_encode($checkResults);
?>